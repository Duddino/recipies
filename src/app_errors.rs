use std::error::Error;
use std::fmt::Display;

#[derive(Debug)]
pub struct ObjectNotFound;

impl Display for ObjectNotFound {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Couldn't find object")
    }
}

impl Error for ObjectNotFound {}
