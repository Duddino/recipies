use crate::app_errors::ObjectNotFound;
use crate::recipe::{Ingredient, Recipe};
use glib::SignalHandlerId;
use gtk::prelude::*;
use std::cell::RefCell;
use std::error::Error;
use std::rc::Rc;

pub fn build_add_recipe(builder: gtk::Builder, recipes: Rc<RefCell<Vec<Recipe>>>) -> Result<(), Box<dyn Error>> {
    let ingredients: gtk::ListBox = builder.get_object("ingredients").ok_or(ObjectNotFound)?;
    let child = ingredient_ui(&builder);
    ingredients.add_child(&builder, &child, None);
    add_signal_to_last_ingredient(builder.clone(), ingredients)?;

    let steps: gtk::ListBox = builder.get_object("steps").ok_or(ObjectNotFound)?;
    let child = step_ui();
    steps.add_child(&builder, &child, None);
    add_signal_to_last_entry(builder.clone(), steps);

    let save_recipe_button: gtk::ToolButton = builder.get_object("save_recipe_button").ok_or(ObjectNotFound)?;
    save_recipe_button.connect_clicked(move |_|{
        save_recipe(&builder, &mut recipes.borrow_mut()).expect("Failed to save recipe");
    });


    Ok(())
}

fn add_signal_to_last_entry(builder: gtk::Builder, steps: gtk::ListBox) {
    let children = steps.get_children();
    children
        .last()
        .unwrap()
        .clone()
        .downcast::<gtk::ListBoxRow>()
        .unwrap()
        .forall(|child| {
            // spoiler: there is only one child
            let child: gtk::Entry = child.clone().downcast().unwrap();

            let handler_id: Rc<RefCell<Option<SignalHandlerId>>> = Rc::new(RefCell::new(None));
            let handler_id2 = handler_id.clone();

            let child_c = child.clone();
            let steps = steps.clone();
            let builder = builder.clone();

            let h_i = child.connect_insert_text(move |_, _, _| {
                let handler_id = handler_id.take();
                child_c.disconnect(handler_id.expect("No handler id."));
                let child = step_ui();
                steps.add_child(&builder, &child, None);
                add_signal_to_last_entry(builder.clone(), steps.clone());
                steps.show_all();
            });

            handler_id2.replace(Some(h_i));
        });
}

fn step_ui() -> gtk::Entry {
    let entry = gtk::Entry::new();
    entry.set_placeholder_text(Some("Add a step..."));
    entry
}

fn save_recipe(builder: &gtk::Builder, recipes: &mut Vec<Recipe>) -> Result<(), Box<dyn Error>>{
    let recipe_name_entry: gtk::Entry = builder.get_object("recipe_name").ok_or(ObjectNotFound)?;
    let recipe_name: String = recipe_name_entry.get_text().into();

    let ingredients: gtk::ListBox = builder.get_object("ingredients").ok_or("ObjectNotFound")?;
    let children = ingredients.get_children();
    let mut ingredients: Vec<Ingredient> = Vec::with_capacity(children.len());
    for child in children {
        let child = child.downcast::<gtk::ListBoxRow>().unwrap();
        let grand_children = child.get_children();
        let grandchild = grand_children.first().ok_or(ObjectNotFound)?.downcast_ref::<gtk::Paned>().unwrap();
        let mut grandgrandchildren = grandchild.get_children().into_iter().map(|g|g.downcast::<gtk::Entry>().unwrap());
        assert!(grandgrandchildren.len() == 2);
        let ing_name = grandgrandchildren.next().unwrap().get_text().into();
        let amount = grandgrandchildren.next().unwrap().get_text().parse();
        if ing_name != "" || amount.is_ok() {
            ingredients.push(Ingredient::new(ing_name, amount?));
        }

    }

    let steps: gtk::ListBox = builder.get_object("steps").ok_or(ObjectNotFound)?;
    let children = steps.get_children();
    let mut steps: Vec<String> = Vec::with_capacity(children.len());
    for child in children {
        let child = child.downcast::<gtk::ListBoxRow>().unwrap();
        let grand_children = child.get_children();
        let grandchild = grand_children.first().ok_or(ObjectNotFound)?.downcast_ref::<gtk::Entry>().unwrap();
        let text = grandchild.get_text().into();
        if text != "" {
            steps.push(text);
        }
    }
    let recipe = Recipe::new(recipe_name, ingredients, steps);
    recipes.push(recipe);
    Recipe::save(recipes, super::DEFAULT_RECIPE_FILE)?;
    super::build_recipe_viewer(builder, &recipes)?;
    builder.get_object::<gtk::Window>("add_recipe_window").ok_or(ObjectNotFound)?.hide();
    Ok(())
}

fn ingredient_ui(builder: &gtk::Builder) -> gtk::Paned {
    let paned = gtk::Paned::new(gtk::Orientation::Horizontal);
    let entry = gtk::Entry::new();
    entry.set_placeholder_text(Some("Ingredient Name"));
    paned.add_child(builder, &entry, None);
    let entry = gtk::Entry::new();
    entry.set_placeholder_text(Some("Amount in grams"));
    paned.add_child(builder, &entry, None);
    paned
}

fn add_signal_to_last_ingredient(builder: gtk::Builder, ingredients: gtk::ListBox) -> Result<(), Box<dyn Error>>{
    let children = ingredients.get_children();
    children
        .last()
        .unwrap()
        .clone()
        .downcast::<gtk::ListBoxRow>()
        .unwrap()
        .forall(|child| {
            // spoiler: there is only one child
            let child: gtk::Paned = child.clone().downcast().unwrap();
            let hello: gtk::Entry = child.get_children()[0].clone().downcast().unwrap();

            let handler_id: Rc<RefCell<Option<SignalHandlerId>>> = Rc::new(RefCell::new(None));
            let handler_id2 = handler_id.clone();

            let child_c = hello.clone();
            let steps = ingredients.clone();
            let builder = builder.clone();

            let h_i = hello.connect_insert_text(move |_, _, _| {
                let handler_id = handler_id.take();
                child_c.disconnect(handler_id.expect("No handler id."));
                let child = ingredient_ui(&builder);
                steps.add_child(&builder, &child, None);
                add_signal_to_last_ingredient(builder.clone(), steps.clone()).unwrap();
                steps.show_all();
            });

            handler_id2.replace(Some(h_i));
        });
    Ok(())
}
