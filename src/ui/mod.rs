mod add_recipe;

use self::add_recipe::build_add_recipe;
use crate::app_errors::ObjectNotFound;
use crate::recipe::Recipe;
use gtk::prelude::*;
use std::error::Error;
use std::fmt::Write;
use std::rc::Rc;
use std::cell::RefCell;

pub const DEFAULT_RECIPE_FILE: &str = "recipes.yaml";

pub fn start_ui() -> Result<(), Box<dyn Error>> {
    gtk::init()?;
    let glade_src = include_str!("main.glade");
    let builder = gtk::Builder::from_string(glade_src);

    let window: gtk::Window = builder.get_object("window").ok_or(ObjectNotFound)?;
    let stack: gtk::Stack = builder.get_object("stack").ok_or(ObjectNotFound)?;
    let back_button: gtk::ToolButton = builder.get_object("back_button").ok_or(ObjectNotFound)?;
    {
        let builder = builder.clone();
        back_button.connect_clicked(move |_| {
            switch_to_viewer(&builder, &stack).expect("Failed to switch to viewer");
        });
    }
    let add_recipe_button: gtk::ToolButton = builder
        .get_object("add_recipe_button")
        .ok_or(ObjectNotFound)?;
    {
        let window: gtk::Window = builder
            .get_object("add_recipe_window")
            .ok_or(ObjectNotFound)?;
        window.connect_delete_event(|window, _| window.hide_on_delete());
        add_recipe_button.connect_clicked(move |_| {
            window.show_all();
        });
    }

    let recipes = Rc::new(RefCell::new(Recipe::load_or_default(DEFAULT_RECIPE_FILE)));
    build_recipe_viewer(&builder, &recipes.borrow()).expect("Failed to build recipe viewer");
    build_add_recipe(builder.clone(), recipes.clone()).expect("Failed to build \"add recipe\"");

    window.show_all();
    window.connect_destroy(|_| {
        gtk::main_quit();
    });
    gtk::main();

    Ok(())
}

pub fn build_recipe_viewer(builder: &gtk::Builder, recipes: &[Recipe]) -> Result<(), Box<dyn Error>> {
    let recipe_viewer: gtk::ListBox = builder.get_object("recipe_viewer").ok_or(ObjectNotFound)?;
    for child in recipe_viewer.get_children() {
        recipe_viewer.remove(&child);
    }
    let stack: gtk::Stack = builder.get_object("stack").ok_or(ObjectNotFound)?;
    for recipe in recipes {
        let button = gtk::Button::with_label(&recipe.name);
        {
            let stack = stack.clone();
            let builder = builder.clone();
            let recipe = recipe.clone();
            button.connect_clicked(move |_| {
                switch_to_recipe(&builder, &stack).expect("Failed to switch to recipe");
                build_recipe(&builder, &recipe).expect("Failed to build recipe");
            });
        }
        recipe_viewer.add_child(builder, &button, None);
    }
    recipe_viewer.show_all();
    Ok(())
}

fn build_recipe(builder: &gtk::Builder, recipe: &Recipe) -> Result<(), Box<dyn Error>> {
    let mut buffer = String::new();

    let recipe_title: gtk::Label = builder.get_object("recipe_title").ok_or(ObjectNotFound)?;
    recipe_title.set_text(&recipe.name);

    let ingredients_box: gtk::Label = builder
        .get_object("recipe_ingredients")
        .ok_or(ObjectNotFound)?;
    for i in recipe.ingredients.iter() {
        writeln!(&mut buffer, "{}: {}g", i.name, i.amount)?;
    }
    ingredients_box.set_text(&buffer);
    buffer.clear();

    let steps_box: gtk::Label = builder.get_object("recipe_steps").ok_or(ObjectNotFound)?;
    for (i, step) in recipe.steps.iter().enumerate() {
        writeln!(&mut buffer, "{}) {}", i + 1, step)?;
    }
    steps_box.set_text(&buffer);
    Ok(())
}

fn switch_to_recipe(builder: &gtk::Builder, stack: &gtk::Stack) -> Result<(), Box<dyn Error>> {
    let rl: gtk::ListBox = builder.get_object("recipe").ok_or(ObjectNotFound)?;
    stack.set_visible_child(&rl);
    Ok(())
}

fn switch_to_viewer(builder: &gtk::Builder, stack: &gtk::Stack) -> Result<(), Box<dyn Error>> {
    let viewer: gtk::ListBox = builder.get_object("recipe_viewer").ok_or(ObjectNotFound)?;
    stack.set_visible_child(&viewer);
    Ok(())
}
