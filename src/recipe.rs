use home::home_dir;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fs::{self, File};
use std::io::{Read, Write};
use std::path::PathBuf;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Recipe {
    pub name: String,
    pub ingredients: Vec<Ingredient>,
    pub steps: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Ingredient {
    pub name: String,
    pub amount: f64, // amount in grams
}

impl Recipe {
    pub fn new(name: String, ingredients: Vec<Ingredient>, steps: Vec<String>) -> Self {
        Self {
            name,
            ingredients,
            steps,
        }
    }
    pub fn load(file_name: &str) -> Result<Vec<Self>, Box<dyn Error>> {
        let mut dir_path = Self::get_store_path().unwrap();
        fs::create_dir_all(&dir_path)?;
        dir_path.push(file_name);
        let mut file = File::open(dir_path)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        Ok(serde_yaml::from_str(&contents)?)
    }

    pub fn load_or_default(file_name: &str) -> Vec<Self> {
        Self::load(file_name).unwrap_or_default()
    }

    pub fn save(recipes: &[Recipe], file_name: &str) -> Result<(), Box<dyn Error>> {
        let mut dir_path = Self::get_store_path().unwrap();
        fs::create_dir_all(&dir_path)?;
        dir_path.push(file_name);
        let mut file = File::create(dir_path)?;
        file.write_all(&serde_yaml::to_string(recipes)?.into_bytes())?;
        Ok(())
    }

    fn get_store_path() -> Option<PathBuf> {
        let mut home = home_dir()?;
        home.push(".local/share/recipies/");
        println!("{:?}", home);
        Some(home)
    }
}

impl Ingredient {
    pub fn new(name: String, amount: f64) -> Self {
        Self { name, amount }
    }
}
