mod app_errors;
mod recipe;
mod ui;

fn main() {
    ui::start_ui().expect("Failed to start ui.");
}
