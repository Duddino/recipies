# Recipies

Simple GTK application that allows you to save recipes and convert between different measuring units (i.e. cups and grams). Currently it can only save and view recipes.

The avatar is licensed under the the Creative Commons Attribution 2.0 Generic license.
    Title: "Pi pumpkin pie"
    Author: "Paul Smith" https://flickr.com/people/37996584546@N01
    Source: "Wikipedia" https://commons.wikimedia.org/wiki/File:Pi_pumpkin_pie,_January_2008.jpg
    License: "CC BY 2.0" https://creativecommons.org/licenses/by/2.0/legalcode
